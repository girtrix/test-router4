
import * as React from "react";
import * as ReactDOM from "react-dom";
import { HashRouter, BrowserRouter } from 'react-router-dom'
// gestione themes per Material UI
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import App from "./containers/App";
import appReducers from "./reducers";

//Redux
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import thunkMiddleware from "redux-thunk";
import * as promiseMiddleware from "redux-promise";

const storeEnhancer = compose(
    applyMiddleware(
        thunkMiddleware,
        promiseMiddleware
    )
);

const store = createStore(
    appReducers, storeEnhancer
);

ReactDOM.render(
    <Provider store={store}>
        <HashRouter>
            <MuiThemeProvider>
                <App />
            </MuiThemeProvider>
        </HashRouter>
    </Provider>,
	document.getElementById("app")
);
