
import { SubmissionError } from 'redux-form';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

const arrayContains = (needle, arrhaystack) => {
    let start = arrhaystack.indexOf(needle);
    return (start > -1);
}

export function loginAsync(values: any): Promise<any>
{
    return sleep(500).then(() => {
        let myTestArray = ['john', 'paul'];
        
        if (!arrayContains(values.username, myTestArray)) {
            throw new SubmissionError({
                username: 'User does not exist',
                _error: 'Async Login failed!'
            });
        } else if (values.password !== 'redux') {
            throw new SubmissionError({
                password: 'Wrong password',
                _error: 'Async Login failed!'
            });
        } else {
            window.alert(`You submitted async:\n\n${JSON.stringify(values, null, 2)}`)
        }
    });
}

export function userValidate(values: any): Promise<any>
{
    return sleep(500).then(() => {
        let myTestArray = ['john', 'paul', 'gerry', 'ringo'];
        if (arrayContains(values.username, myTestArray)) {
            throw { username: 'That username is taken' };
        }
    });
}
