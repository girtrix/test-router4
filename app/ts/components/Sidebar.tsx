
import * as React from 'react';
import { Link, NavLink } from 'react-router-dom';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import createBrowserHistory from 'history/createBrowserHistory';
import RaisedButton  from 'material-ui/RaisedButton';

export default class Sidebar extends React.Component<any, any>
{
    /*
    componentWillMount() {
        this.setState({isOpen: true});
    }*/

    // N.B: bisogna usare un arrow function perchè altrimenti i "Non react methods" non vedono l'oggetto "this" !!
    closeMenu = () => {
        this.props.store.toggleMenuOpen(false);

        //let myhistory = createBrowserHistory();
        //myhistory.push('/bolla');
    };

    render() {
        const { isMenuOpen } = this.props.store.state;

		return (
            <Drawer open={isMenuOpen} docked={false}>
                <Link to='/'><MenuItem onClick={this.closeMenu}>Home</MenuItem></Link>
                <Link to='/bolla'><MenuItem onClick={this.closeMenu}>Test Operatore/Bolla</MenuItem></Link>
                <Link to='/simpleform'><MenuItem onClick={this.closeMenu}>Simple Form</MenuItem></Link>
                <Link to='/syncform'><MenuItem onClick={this.closeMenu}>Sync Valid Form</MenuItem></Link>
                <Link to='/asyncform'><MenuItem onClick={this.closeMenu}>Async Valid Form</MenuItem></Link>
                <Link to='/fieldvalid'><MenuItem onClick={this.closeMenu}>Field Valid Form</MenuItem></Link>
                <Link to='/notexists'><MenuItem onClick={this.closeMenu}>link error</MenuItem></Link>

                <RaisedButton label="<< chiudi" onClick={this.closeMenu} />
            </Drawer>
		);
	}
}
