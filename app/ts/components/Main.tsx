
import * as React from 'react';
import { Switch, Route } from 'react-router-dom'

import Home from './views/Home';
import SimpleForm from './views/SimpleForm';
import SyncForm from './views/SyncForm';
import AsyncForm from './views/AsyncForm';
import FieldValidForm from './views/FieldValidForm';
import NotFound from './views/NotFound';
import Bolla from './views/Bolla';

export default class Main extends React.Component<{}, {}>
{
    render() {
		return (
			<main>
                <Switch>
                    <Route exact path='/' component={Home}/>
                    <Route path='/bolla' component={Bolla}/>
                    <Route path='/simpleform' component={SimpleForm}/>
                    <Route path='/syncform' component={SyncForm}/>
                    <Route path='/fieldvalid' component={FieldValidForm}/>
                    <Route path='/asyncform' component={AsyncForm}/>
                    <Route component={NotFound}/>
                </Switch>
            </main>
		);
	}
}
