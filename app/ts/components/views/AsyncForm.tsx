
import * as React from 'react';
import { Field, reduxForm } from 'redux-form';
import { userValidate } from '../../services/FormController';

const validate = values => {
    const errors: any = {};
    if (!values.username) {
      errors.username = 'Required';
    }
    if (!values.password) {
      errors.password = 'Required';
    }
    return errors;
}

const renderField = ({ input, label, type, meta: { asyncValidating, touched, error } }) => (
    <div>
        <label>{label}</label>
        <div className={asyncValidating ? 'async-validating' : ''}>
            <input {...input} type={type} placeholder={label} />
            {touched && error && <span>{error}</span>}
        </div>
    </div>
);

export class AsyncForm extends React.Component<any, any>
{
    private mySubmit = (values) => {
        // NB: devo passare una function su "handleSubmit" nel caso il "componente form" sia il contenitore principale!
    }

    render() {
        const { handleSubmit, pristine, reset, submitting } = this.props;

        return (
            <form onSubmit={handleSubmit(this.mySubmit)}>
                <Field
                    name="username"
                    type="text"
                    component={renderField}
                    label="Username"
                />
                <Field
                    name="password"
                    type="password"
                    component={renderField}
                    label="Password"
                />
                <div>
                    <button type="submit" disabled={submitting}>Sign Up</button>
                    <button type="button" disabled={pristine || submitting} onClick={reset}>Clear Values</button>
                </div>
            </form>
        );
    }
}

export default reduxForm({
    form: 'asyncForm', // a unique identifier for this form
    validate: validate,
    asyncValidate: userValidate,
    asyncBlurFields: ['username']
})(AsyncForm)
