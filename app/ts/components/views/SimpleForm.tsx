
import * as React from 'react';
import { Field, reduxForm } from 'redux-form';
import { SubmissionError } from 'redux-form';
import { loginAsync } from '../../services/FormController';
import { renderTextField } from '..//MaterialUIRender';
import RaisedButton  from 'material-ui/RaisedButton';

/*
const renderField = ({ input, label, type, meta: { touched, error } }) => (
    <div>
        <label>{label}</label>
        <div>
            <input {...input} placeholder={label} type={type} />
            {touched && error && <span>{error}</span>}
        </div>
    </div>
);
*/

export class SimpleForm extends React.Component<any, any>
{
    private arrayContains(needle, arrhaystack)  {
        let start = arrhaystack.indexOf(needle);
        return (start > -1);
    }

    private submitSync = (values) => {
        let myTestArray = ['john', 'paul'];

        if (!this.arrayContains(values.username, myTestArray)) {
            throw new SubmissionError({
                username: 'User does not exist',
                _error: 'Login failed!'
            });
        } else if (values.password !== 'redux') {
            throw new SubmissionError({
                password: 'Wrong password',
                _error: 'Login failed!'
            });
        } else {
            window.alert(`You submitted:\n\n${JSON.stringify(values, null, 2)}`);
            this.props.history.push('/'); //vado a un'altra route!!
        }
    }

    render() {
        const { error, handleSubmit, pristine, reset, submitting } = this.props;

        return (
            /* <form onSubmit={handleSubmit(this.submitSync)}> 
            <form onSubmit={handleSubmit(loginAsync)}> */
            <form onSubmit={handleSubmit(this.submitSync)}>
                <div><Field name="username" component={renderTextField} label="Username" /></div>
                <div><Field name="password" type="password" component={renderTextField} label="Password" /></div>
                {error && <strong>{error}</strong>}
                <div>
                    <RaisedButton type="submit" disabled={submitting}>Log In</RaisedButton>&nbsp;&nbsp;&nbsp;
                    <RaisedButton type="button" disabled={pristine || submitting} onClick={reset}>Clear Values</RaisedButton>
                </div>
            </form>
        );
    }
}

export default reduxForm({
    form: 'simpleForm' // a unique identifier for this form
})(SimpleForm)
