
import * as React from 'react';
import {Card, CardActions, CardText} from 'material-ui/Card';
import RaisedButton  from 'material-ui/RaisedButton';
import { Link } from 'react-router-dom';

const styles = {
    root: {
        display: 'flex',
        flexWrap: 'wrap' as 'wrap', // senza l'uso del "cast to type" va in errore, non compila!
        justifyContent: 'center' as 'center'
    },
    gridList: {
        width: 500,
        height: 450,
        overflowY: 'auto',
    },
};

const tilesData = [
    {
        id: 'btnInizio',
        caption: 'Inizio Lav.',
        descr: 'Dichiarazione di inizio lavorazione di fase.',
        url: '/bolla'
    },
    {
        id: 'btnFine',
        caption: 'Fine Lav.',
        descr: 'Dichiarazione di fine lavoro.',
        url: '/roster'
    },
    {
        id: 'btnVers',
        caption: 'Versa Prodotto',
        descr: 'Dichiarazione di versamento prodotto finito',
        url: '/schedule'
    }
];

export default class Home extends React.Component<any, any>
{
    cardSelect = () => {
        let a = 1;
        let b = a +1;
    }

    render() {
        return (
            <div style={styles.root}> 
                {tilesData.map((tile, index) => (
                    <Card key={index} style={{maxWidth: '200px', margin: 10}}>
                        <CardText>{tile.descr}</CardText>
                        <CardActions>
                            <Link to={tile.url}>
                                <RaisedButton label={tile.caption} fullWidth={true} onClick={this.cardSelect} />
                            </Link>
                        </CardActions>
                    </Card>
                ))}
            </div>
        );
    }
}
