
import * as React from 'react';
import { Field, reduxForm } from 'redux-form';

const validate = values => {
    const errors: any = {};

    if (!values.username) {
        errors.username = 'Required';
    } else if (values.username.length > 15) {
        errors.username = 'Must be 15 characters or less';
    }

    if (!values.email) {
        errors.email = 'Required';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Invalid email address';
    }

    if (!values.age) {
        errors.age = 'Required';
    } else if (isNaN(Number(values.age))) {
        errors.age = 'Must be a number';
    } else if (Number(values.age) < 18) {
        errors.age = 'Sorry, you must be at least 18 years old';
    }

    return errors;
}
  
const warn = values => {
    const warnings: any = {};
    if (values.age < 19) {
        warnings.age = 'Hmm, you seem a bit young...';
    }
    return warnings;
}
  
const renderField = ({ input, label, type, meta: { touched, error, warning } }) => (
    <div>
        <label>{label}</label>
        <div>
            <input {...input} placeholder={label} type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    </div>
);

export class SyncForm extends React.Component<any, any>
{
    private mySubmit = (values) => {
        // NB: devo passare una function su "handleSubmit" nel caso il "componente form" sia il contenitore principale!
    }

    render() {
        const { handleSubmit, pristine, reset, submitting } = this.props;

        return (
            <form onSubmit={handleSubmit(this.mySubmit)}>
                <Field
                    name="username"
                    type="text"
                    component={renderField}
                    label="Username"
                />
                <Field name="email" type="email" component={renderField} label="Email" />
                <Field name="age" type="number" component={renderField} label="Age" />
                <div>
                    <button type="submit" disabled={submitting}>
                        Submit
                    </button>
                    <button type="button" disabled={pristine || submitting} onClick={reset}>
                        Clear Values
                    </button>
                </div>
            </form>
        );
    }
}

export default reduxForm({
    form: 'syncForm', // a unique identifier for this form
    validate: validate, // <--- validation function given to redux-form
    warn: warn // <--- warning function given to redux-form
})(SyncForm)