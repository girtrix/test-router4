
import * as React from 'react';

export default class Schedule extends React.Component<{}, {}>
{
    render() {
        return (
            <div>
                <h3>Schedule</h3>
                <ul>
                    <li>6/5 @ Evergreens</li>
                    <li>6/8 vs Kickers</li>
                    <li>6/14 @ United</li>
                </ul>
            </div>
        );
    }
}
