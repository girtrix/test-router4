
import { handleActions, Action } from "redux-actions";
import {IAppState, IBollaLav} from "../interfaces";
import * as co from "../constants";

const defaultState: IAppState = {
    isMenuOpen: false,
	operatore: undefined,
	bolla: undefined,
	infoMessage: "",
	errorMessage: "",
    componenti: []
};

const app = handleActions<IAppState>({
	[co.TOGGLE_MAIN_MENU]: (state, action) => ({
		...state, isMenuOpen: action.payload
	}),
    [co.SET_INFO_MSG]: (state, action) => ({
        ...state, infoMessage: action.payload
    }),
    [co.SET_ERROR_MSG]: (state, action) => ({
        ...state, errorMessage: action.payload
    }),
	[co.SET_OPERATORE]: (state, action) => {
		if (action.payload.error != '') {
			return { ...state, errorMessage: `${action.payload.error}` };
		}
		return { ...state, errorMessage: '', operatore: action.payload };
	},
    [co.SET_BOLLA]: (state, action: Action<IBollaLav>) => {
        if (action.payload.error != '') {
            return { ...state, errorMessage: `${action.payload.error}` };
        }
        return { ...state, errorMessage: '', bolla: action.payload };
    },
    [co.FETCH_COMPONENTI]: (state, action) => ({
        ...state, componenti: action.payload
    })

}, defaultState );

export default app;
