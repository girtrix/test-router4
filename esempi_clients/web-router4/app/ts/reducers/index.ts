
import { combineReducers } from "redux";
import { reducer as formReducer } from 'redux-form'
import app from "./app";

const reducer = combineReducers({
    state: app, //per esporre IAppState come "state" !
    form: formReducer
});
export default reducer;
