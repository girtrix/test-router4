
import { Action } from "redux-actions";

export interface  IOperatore
{
    badge: string;
    codice: string;
    descrizione: string;
    error: string;
}

export interface  IBollaLav
{
    anno: number;
    numero: number;
    commessa: string;
    articolo: string;
    error: string;
}

export interface IComponente
{
    codice: string;
    descrizione: string;
    qtaTot: number;
    qtaRes: number;
    um: string;
}

export interface IAppState
{
    isMenuOpen: boolean;
    operatore: IOperatore;
    bolla: IBollaLav;
    componenti: IComponente[];
    errorMessage: string;
    infoMessage: string;
}

export interface IAppActions
{
    toggleMenuOpen: (toggle: boolean) => Action<boolean>;
    setInfoMessage: (msg: string) => Action<string>;
    setErrorMessage: (msg: string) => Action<string>;
    setOperatore: (badge: string) => Promise<IOperatore>;
    setBolla: (anno: number, numero: number) => Promise<IBollaLav>;
    fetchComponenti: (bolla: IBollaLav) => Promise<IComponente[]>;
}

/*
export interface IRootState
{
    state: IAppState;
}
*/