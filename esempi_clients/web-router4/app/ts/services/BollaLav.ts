
import {IBollaLav, IComponente} from "../interfaces";

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

export function getBolla(anno: number, numero: number): Promise<IBollaLav>
{
    const annoSel = anno;
    const numSel = numero;

    return sleep(1000).then(() => {
        let bolla: IBollaLav = {
            anno: annoSel,
            numero: numSel,
            articolo: 'A1-PF',
            commessa: 'CPI/2018/0000001',
            error: ''
        };
        return bolla;
    });
}

export function fetchComponenti(anno: number, numero: number): Promise<IComponente[]>
{
    return sleep(1000).then(() => {
        // componente 1
        let comp_01: IComponente = {
            codice: 'ART001',
            descrizione: 'primo componente',
            qtaTot: 6,
            qtaRes: 5,
            um: 'NR'
        };

        // componente 2
        let comp_02: IComponente = {
            codice: 'ART002',
            descrizione: 'secondo componente',
            qtaTot: 10,
            qtaRes: 8,
            um: 'NR'           
        };

        // componente 3
        let comp_03: IComponente = {
            codice: 'ART003',
            descrizione: 'terzo componente',
            qtaTot: 12,
            qtaRes: 8,
            um: 'NR'           
        };

        let componenti: Array<IComponente> = [comp_01, comp_02, comp_03];

        return componenti;
    });
}