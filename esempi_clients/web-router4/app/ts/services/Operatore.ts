
import { IOperatore } from "../interfaces";

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

export function getOperatore(badge: string): Promise<IOperatore>
{
    return sleep(1000).then((badge: string) => {
        const op: IOperatore = {
            badge: badge,
            codice: '0001',
            descrizione: 'Gerry',
            error: ''
        };
        return op;
    });
}
