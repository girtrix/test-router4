
import { createAction } from "redux-actions";
import * as co from "../constants";
import { IOperatore, IBollaLav, IComponente } from "../interfaces";
import { getOperatore } from "../services/Operatore";
import { getBolla, fetchComponenti } from "../services/BollaLav";

const appActions = {
    toggleMenuOpen: createAction<boolean, boolean>(co.TOGGLE_MAIN_MENU, (toggle: boolean) => toggle),
    setInfoMessage: createAction<string, string>(co.SET_INFO_MSG, (msg: string) => msg),
    setErrorMessage: createAction<string, string>(co.SET_ERROR_MSG, (msg: string) => msg),

    setOperatore: createAction<Promise<IOperatore>, string>(co.SET_OPERATORE, async (badge: string) => await getOperatore(badge)),
    setBolla: createAction<Promise<IBollaLav>, number, number>(co.SET_BOLLA, async (anno: number, numero: number) => await getBolla(anno, numero)),
    fetchComponenti: createAction<Promise<IComponente[]>, number, number>(co.FETCH_COMPONENTI, async (anno: number, numero: number) => await fetchComponenti(anno, numero))
};

export default appActions;
