
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';
import { IAppState } from "../interfaces";
import actions from "../actions/actions";

import Header from '../components/Header';
import Header2 from '../components/Header2';
import Sidebar from '../components/Sidebar';
import Main from '../components/Main';

// mapping state to the props
const mapStateToProps = (state: IAppState) => state;

// mapping actions to the props
const mapDispatchToProps = {
    ...actions
};

export class App extends React.Component<any, any>
{
	render() {
		return (
            <div>
                <Header store={this.props} />
                <Header2 />
                <Sidebar store={this.props} />
                <Main />
            </div>
		);
	}
}

// connect store to App
export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(App));
