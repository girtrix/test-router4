
import * as React from 'react';
import { fetchComponenti } from "../../services/BollaLav";

export default class Lista extends React.Component<any, any>
{
    constructor(props: any){
        super(props);
        this.state = {components: []};
    }

    componentDidMount() {
        this.getComps();
    }

    async getComps() {
        let comps = await fetchComponenti(2018, 1);
        this.setState({components: comps});
    }

    render() {
        let items = this.state.components;
        return (
            <div>
                {items.map((comp) => (
                    <div>{comp.codice}</div>
                ))}
            </div>
        );
    }
}
