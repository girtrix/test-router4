
import * as React from 'react';
import { connect } from "react-redux";
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import actions from "../../actions/actions";
import { IAppState } from "../../interfaces";

const mapStateToProps = (state: IAppState) => state;

// mapping actions to the props
const mapDispatchToProps = {
    ...actions
};

export class Bolla extends React.Component<any, any>
{
    private onValidOperatore = () => {
        let objBadge = document.getElementById('oper') as any;
        let badge = objBadge.value;
        this.props.setOperatore(badge);

        //this.refs.myTextField.input.value
        //this.refs.myTextField.getValue
    };

    private onValidBolla = () => {
        //this.props.store.toggleMenuOpen(true);
    };
    
    render() {
        const oper = this.props.state.operatore;

        return (
            <div>
                <div>
                    Operatore <TextField id="oper" hintText="cod. badge" />
                    <RaisedButton label="Operatore" onClick={this.onValidOperatore} />
                    <p>codice: {oper ? oper.codice : ''}</p>
                </div>
                <Divider />
                <div>
                    Bolla <br/>
                    <TextField hintText="anno" /><br/>
                    <TextField hintText="numero" /><br/>
                    <RaisedButton label="Bolla" onClick={this.onValidBolla} />
                </div>
            </div>
        );
    }
}

// connect store to App
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Bolla);
