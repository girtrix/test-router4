
import * as React from 'react';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import Toggle from 'material-ui/Toggle';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import MenuIcon from 'material-ui/svg-icons/navigation/menu';
import NavigationClose from 'material-ui/svg-icons/navigation/close';

const Logged = (props) => (
    <IconMenu
      {...props}
      iconButtonElement={
        <IconButton><MoreVertIcon /></IconButton>
      }
      targetOrigin={{horizontal: 'right', vertical: 'top'}}
      anchorOrigin={{horizontal: 'right', vertical: 'top'}}
    >
      <MenuItem primaryText="Refresh" />
      <MenuItem primaryText="Help" />
      <MenuItem primaryText="Sign out" />
    </IconMenu>
);

const mystyle = {background: '#414346'};

//Logged.muiName = 'IconMenu';

export default class Header extends React.Component<any, any>
{
    private onToggleMenu = () => {
        this.props.store.toggleMenuOpen(true);
    };

    render() {
        return (
            <header>
                <AppBar
                    title="Title"
                    iconElementLeft={<IconButton onClick={this.onToggleMenu}><MenuIcon /></IconButton>}
                    iconElementRight={<Logged />}
                    style={mystyle}
                />
                
            </header>
        );
    }
}
