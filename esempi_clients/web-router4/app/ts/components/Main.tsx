
import * as React from 'react';
import { Switch, Route } from 'react-router-dom'

import Home from './views/Home';
import Roster from './views/Roster';
import Schedule from './views/Schedule';
import NotFound from './views/NotFound';
import Bolla from './views/Bolla';
import Lista from './views/Lista';

export default class Main extends React.Component<{}, {}>
{
    render() {
		return (
			<main>
                <Switch>
                    <Route exact path='/' component={Home}/>
                    <Route path='/roster' component={Roster}/>
                    <Route path='/schedule' component={Schedule}/>
                    <Route path='/bolla' component={Bolla}/>
                    <Route path='/lista' component={Lista}/>
                    <Route component={NotFound}/>
                </Switch>
            </main>
		);
	}
}
